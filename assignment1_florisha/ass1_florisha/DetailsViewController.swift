
import UIKit
import MapKit

class DetailsViewController: UIViewController {

    var place: PlaceOfInterest?
    @IBOutlet weak var mapVIew: MKMapView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let value = place?.photo_reference {
            imageView.downloadedFrom(url: Constants.getUrl(photoReference: value))
            }

        updateDetails()
        setMapViewCoordinate()
    }
    func updateDetails() -> Void {
        
        guard let place = self.place else { return }
        if let placeId = place.place_id {
            GooglePlaceAPI.details(placeId: placeId, completionHandler: {(status, json) in
                if let jsonObj = json {
                    if let value = APIParser.detailsParse(json: jsonObj) {
                        
                        
                        if let phone = value.phone, let website = value.website {
                            self.addressLabel.text = "\(phone)\n\(website)"
                          
                        }
                    }
                    
                    
                    
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation, name: String) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapVIew.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        annotation.title = name
        mapVIew.addAnnotation(annotation)
        
    }
    
    func setMapViewCoordinate(){
        
        mapVIew.delegate = self
        
        if let coordinate = place?.location?.coordinate {
            let annotation = MKPointAnnotation()
            annotation.title = place?.name
            annotation.coordinate.latitude = coordinate.latitude
            annotation.coordinate.longitude =  coordinate.longitude
            mapVIew.addAnnotation(annotation)
            centerMapOnLocation(annotation.coordinate)
            mapVIew.showsUserLocation = true
            
          
        }
    }
    
    func centerMapOnLocation(_ location: CLLocationCoordinate2D) {
        let radius = 5000
        let region = MKCoordinateRegionMakeWithDistance(location, CLLocationDistance(Double(radius) * 2.0), CLLocationDistance(Double(radius) * 2.0))
        mapVIew.setRegion(region, animated: true)
    }

}
extension DetailsViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        //if user's current location (blue dot), ignore
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        //otherwise, customizing pin on the map for a given l=point.
        let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "reusePin")
        //allowing to show extra information in the pin view
        view.canShowCallout = true
        //"i" button
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
        view.pinTintColor = UIColor.blue
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //accessory (by default - "i" button on your annotation view.
        let location = view.annotation
        //preparation for a mode in Apple Maps
        let launchingOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking]
        if let coordinate = view.annotation?.coordinate {
            //open apple maps with the required coordinate and launching options - walking mode
            
            //            location?.mapItem(coordinate:coordinate).openInMaps(launchOptions: launchingOptions)
            //            location.ma
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
//            mapItem.name = "\(self.place!.name)"
            mapItem.openInMaps(launchOptions: launchingOptions)
        }
    }
}
