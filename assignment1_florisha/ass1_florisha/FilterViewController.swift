
import UIKit


protocol FilterViewDelegate {
    func getQueryItems(rankBy: String,distance: String, openNow: Bool)
}

class FilterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var rankBy = "Prominence"
    var distance = "25000"

    let rankValues = ["Prominence", "Distance"]
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.dataSource = self
        pickerView.delegate = self
    }
    var delegate: FilterViewDelegate?

    @IBAction func applyAction(_ sender: Any) {
        delegate?.getQueryItems(rankBy: rankBy, distance: distance, openNow: openNowSwitch.isOn)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var openNowSwitch: UISwitch!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var pickerView: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rankValues.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rankValues[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rankBy = rankValues[row]
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        distanceLbl.text = "\(Int(ceil(sender.value * 50)))km"
        distance = "\(Int(ceil(sender.value * 50000)))"
    }
}
