//
//  PlaceOfInterest.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-18.
//  Copyright © 2018 Jinal. All rights reserved.
//

import Foundation
import CoreLocation
class PlaceOfInterest {
    
    var id: String
    var name: String
    var rating: Double?
    var formattedAddress: String?
    var icon: String?
    var photo_reference: String?
    var lat: Double?
    var lng: Double?
    var place_id: String?
    var location: CLLocation?
    
    
    
    init? (json:[String: Any]){
        
        
        
        guard let id = json["id"] as? String  else {
            return nil
        }
        
        guard let name = json["name"] as? String  else {
            return nil
        }
        
        self.id = id
        self.name = name
        
        if let rat = json["rating"] as? Double {
            self.rating = rat
        } else {
            self.rating = 0
        }
        self.place_id = json["place_id"] as? String
        self.formattedAddress = json["formatted_address"] as? String
        
        let image = json["photos"] as? [[String: Any]]
        self.photo_reference = image![0]["photo_reference"] as? String
        self.icon = json["icon"] as? String
        
        if let geometry = json["geometry"] as? [String: Any] {
            if let locationCoordinate = geometry["location"] as? [String: Double
                ] {
                if locationCoordinate["lat"] != nil, locationCoordinate["lng"] != nil {
                    self.location = CLLocation(latitude: locationCoordinate["lat"]!, longitude: locationCoordinate["lng"]!)
                }
            }
        }
    }
}
class DetailsPlace: NSObject {
    var phone: String?
    var website: String?
    
    init?(json: [String: Any]){
        
        self.phone = json["formatted_phone_number"] as? String
        self.website = json["website"] as? String
        
        
    }
}
