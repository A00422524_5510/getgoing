import UIKit

class SearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var places: [PlaceOfInterest]!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if places != nil {
                places = places.sorted {
                    $0.name < $1.name
                }
            }
        } else {
            if places != nil {
                places = places.sorted {
                    $0.rating! < $1.rating!
                }
            }
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if places != nil {
            places = places.sorted {
                $0.name < $1.name
            }
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView Delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchPlaceCell
//        cell.place = places[indexPath.row]
        cell.nameLabel.text = places[indexPath.row].name
//        print("\(places[indexPath.row].rating)")
        if let value = places[indexPath.row].rating {
            cell.ratingLabel.text = "\(value)"
        }
        cell.addressLabel.text = places[indexPath.row].formattedAddress
        
        cell.photo.downloadedFrom(link: places[indexPath.row].icon!)
//        print(places[indexPath.row].formattedAddress)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        details.place = places[indexPath.row]
        self.navigationController?.pushViewController(details, animated: true)
    }
    

}
