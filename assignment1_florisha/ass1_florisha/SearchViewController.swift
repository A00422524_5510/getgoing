//
//  UIViewController.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-13.
//  Copyright © 2018 Jinal. All rights reserved.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController {
    var searchParam: String?
    
    var rankBy = "Prominence"
    var distance = "25000"
    var openNow = false
    var currentLocation: CLLocation?
    
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchParameterTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var searchParameterTextField: UITextField!
    
      @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  MARK: - Navigation
    @IBAction func searchButtonAction(_ sender: Any) {
        print("button clicked")
        
        searchParameterTextField.resignFirstResponder()
        
        if let inputValue = searchParam, inputValue.count > 0 {
            print ("success!")
            if segmentedControl.selectedSegmentIndex == 0 {
                GooglePlaceAPI.textSearch(query: inputValue, rank: rankBy, distance: distance, openNow: openNow){ ( statusCode, json) in
                    if let jsonObj = json{
                        let places = APIParser.parseAPIResponse(json: jsonObj)
                        
                        DispatchQueue.main.async {
                            let resultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
                            resultsViewController.places = places
                            self.navigationController?.pushViewController(resultsViewController, animated: true)
                            
                            
                        }
                        print("\(places.count)")
                    }else {
                        self.generalAlert(title: "Opps", message: "An Error in parsing json")
                    }
                }
            } else {
                guard let location = currentLocation else { return }
                GooglePlaceAPI.locationSearch(lat: location.coordinate.latitude, lng: location.coordinate.longitude, rank: rankBy, distance: distance, openNow: openNow){ ( statusCode, json) in
                    if let jsonObj = json{
                        let places = APIParser.parseAPIResponse(json: jsonObj)

                        DispatchQueue.main.async {
                            let resultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
                            resultsViewController.places = places
                            self.navigationController?.pushViewController(resultsViewController, animated: true)


                        }
                        print("\(places.count)")
                    }else {
                        self.generalAlert(title: "Opps", message: "An Error in parsing json")
                    }
                }
            }
           
            
            
            //            generalAlert(title:"success!" , message: "we got \(inputValue)")
        }else{
            alertError()
        }
        
    }
    
    func alertError(){
        let alertController = UIAlertController(title: "Opps", message: "An Error...", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        
        present(alertController, animated: true) {
            self.searchParameterTextField.placeholder = "Input something"
        }
    }
    
  
    

    
    
    func generalAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        
        present(alertController, animated: true) {
            self.searchParameterTextField.placeholder = "Input something"
        }
    }
    
    
    @IBAction func filterAction(_ sender: Any) {
        let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterViewController.delegate = self
        self.present(filterViewController, animated: true, completion: nil)
    }
    
    @IBAction func sgSelectionChanged(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 1 {
            LocationService.sharedInstance.delegate = self
            LocationService.sharedInstance.startUpdatingLocation()
        }
    }
    
}

extension SearchViewController: UITextFieldDelegate, FilterViewDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchParameterTextField{
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        if textField == searchParameterTextField{
            searchParam = textField.text
            print(textField.text ?? "No Value")
        }
        return true
    }
    func getQueryItems(rankBy: String, distance: String, openNow: Bool) {
        self.rankBy = rankBy
        self.distance = distance
        self.openNow = openNow
    }
}
extension SearchViewController: LocationServiceDelegate {
    func tracingLocation(_ currentLocation: CLLocation) {
        self.currentLocation = currentLocation
        print("\(currentLocation.coordinate.latitude) \(currentLocation.coordinate.longitude)")
    }
    
    func tracingLocationDidFailWithError(_ error: Error) {
        
    }
    
}

